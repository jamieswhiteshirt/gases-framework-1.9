package com.jamieswhiteshirt.util.orientation;

import net.minecraft.entity.Entity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.IStringSerializable;

public enum EnumOrientation implements IStringSerializable {
    NORTH_FORWARD(Yaw.NORTH, Pitch.FORWARD, new EnumFacing[] {
            EnumFacing.DOWN, EnumFacing.UP, EnumFacing.NORTH, EnumFacing.SOUTH, EnumFacing.WEST, EnumFacing.EAST
    }),
    WEST_FORWARD(Yaw.WEST, Pitch.FORWARD, new EnumFacing[] {
            EnumFacing.DOWN, EnumFacing.UP, EnumFacing.WEST, EnumFacing.EAST, EnumFacing.SOUTH, EnumFacing.NORTH
    }),
    SOUTH_FORWARD(Yaw.SOUTH, Pitch.FORWARD, new EnumFacing[] {
            EnumFacing.DOWN, EnumFacing.UP, EnumFacing.SOUTH, EnumFacing.NORTH, EnumFacing.EAST, EnumFacing.WEST
    }),
    EAST_FORWARD(Yaw.EAST, Pitch.FORWARD, new EnumFacing[] {
            EnumFacing.DOWN, EnumFacing.UP, EnumFacing.EAST, EnumFacing.WEST, EnumFacing.NORTH, EnumFacing.SOUTH
    }),
    NORTH_DOWNWARD(Yaw.NORTH, Pitch.DOWNWARD, new EnumFacing[] {
            EnumFacing.SOUTH, EnumFacing.NORTH, EnumFacing.DOWN, EnumFacing.UP, EnumFacing.WEST, EnumFacing.EAST
    }),
    WEST_DOWNWARD(Yaw.WEST, Pitch.DOWNWARD, new EnumFacing[] {
            EnumFacing.EAST, EnumFacing.WEST, EnumFacing.DOWN, EnumFacing.UP, EnumFacing.SOUTH, EnumFacing.NORTH
    }),
    SOUTH_DOWNWARD(Yaw.SOUTH, Pitch.DOWNWARD, new EnumFacing[] {
            EnumFacing.NORTH, EnumFacing.SOUTH, EnumFacing.DOWN, EnumFacing.UP, EnumFacing.EAST, EnumFacing.WEST
    }),
    EAST_DOWNWARD(Yaw.EAST, Pitch.DOWNWARD, new EnumFacing[] {
            EnumFacing.WEST, EnumFacing.EAST, EnumFacing.DOWN, EnumFacing.UP, EnumFacing.NORTH, EnumFacing.SOUTH
    }),
    NORTH_BACKWARD(Yaw.NORTH, Pitch.BACKWARD, new EnumFacing[] {
            EnumFacing.UP, EnumFacing.DOWN, EnumFacing.SOUTH, EnumFacing.NORTH, EnumFacing.WEST, EnumFacing.EAST
    }),
    WEST_BACKWARD(Yaw.WEST, Pitch.BACKWARD, new EnumFacing[] {
            EnumFacing.UP, EnumFacing.DOWN, EnumFacing.EAST, EnumFacing.WEST, EnumFacing.SOUTH, EnumFacing.NORTH
    }),
    SOUTH_BACKWARD(Yaw.SOUTH, Pitch.BACKWARD, new EnumFacing[] {
            EnumFacing.UP, EnumFacing.DOWN, EnumFacing.NORTH, EnumFacing.SOUTH, EnumFacing.EAST, EnumFacing.WEST
    }),
    EAST_BACKWARD(Yaw.EAST, Pitch.BACKWARD, new EnumFacing[] {
            EnumFacing.UP, EnumFacing.DOWN, EnumFacing.WEST, EnumFacing.EAST, EnumFacing.NORTH, EnumFacing.SOUTH
    }),
    NORTH_UPWARD(Yaw.NORTH, Pitch.UPWARD, new EnumFacing[] {
            EnumFacing.NORTH, EnumFacing.SOUTH, EnumFacing.UP, EnumFacing.DOWN, EnumFacing.WEST, EnumFacing.EAST
    }),
    WEST_UPWARD(Yaw.WEST, Pitch.UPWARD, new EnumFacing[] {
            EnumFacing.WEST, EnumFacing.EAST, EnumFacing.UP, EnumFacing.DOWN, EnumFacing.SOUTH, EnumFacing.NORTH
    }),
    SOUTH_UPWARD(Yaw.SOUTH, Pitch.UPWARD, new EnumFacing[] {
            EnumFacing.SOUTH, EnumFacing.NORTH, EnumFacing.UP, EnumFacing.DOWN, EnumFacing.EAST, EnumFacing.WEST
    }),
    EAST_UPWARD(Yaw.EAST, Pitch.UPWARD, new EnumFacing[] {
            EnumFacing.EAST, EnumFacing.WEST, EnumFacing.UP, EnumFacing.DOWN, EnumFacing.NORTH, EnumFacing.SOUTH
    });

    private static final EnumOrientation[][] yawPitchLookup = new EnumOrientation[4][4];

    static {
        for (EnumOrientation orientation : values()) {
            yawPitchLookup[orientation.yaw.getOrientationIndex()][orientation.pitch.getOrientationIndex()] = orientation;
        }
    }

    public static EnumOrientation fromOrientationIndex(int orientationIndex) {
        return values()[orientationIndex & 15];
    }

    public static EnumOrientation fromYawPitch(Yaw yaw, Pitch pitch) {
        return yawPitchLookup[yaw.getOrientationIndex()][pitch.getOrientationIndex()];
    }

    public static EnumOrientation fromEntity(Entity entity) {
        return fromYawPitch(Yaw.fromEntity(entity), Pitch.fromEntity(entity));
    }

    private final String name;
    public final Yaw yaw;
    public final Pitch pitch;
    private final EnumFacing[] localToWorldTransformation = new EnumFacing[6];
    private final EnumLocalFacing[] worldToLocalTransformation = new EnumLocalFacing[6];

    EnumOrientation(Yaw yaw, Pitch pitch, EnumFacing[] localToWorldTransformation) {
        this.name = yaw.getName() + "_" + pitch.getName();
        this.yaw = yaw;
        this.pitch = pitch;

        for (EnumLocalFacing localFacing : EnumLocalFacing.values()) {
            EnumFacing worldFacing = localToWorldTransformation[localFacing.getIndex()];
            this.localToWorldTransformation[localFacing.getIndex()] = worldFacing;
            this.worldToLocalTransformation[worldFacing.getIndex()] = localFacing;
        }
    }

    public int getOrientationIndex() {
        return this.ordinal();
    }

    public EnumFacing localToWorld(EnumLocalFacing localFacing) {
        return this.localToWorldTransformation[localFacing.getIndex()];
    }

    public EnumLocalFacing worldToLocal(EnumFacing worldFacing) {
        return this.worldToLocalTransformation[worldFacing.getIndex()];
    }

    public EnumOrientation getOpposite() {
        return fromYawPitch(yaw.getOpposite(), pitch.getOpposite());
    }

    @Override
    public String toString() {
        return this.name;
    }

    @Override
    public String getName() {
        return this.name;
    }



    public enum Yaw implements IStringSerializable {
        NORTH("north"),
        WEST("west"),
        SOUTH("south"),
        EAST("east");

        private final String name;

        public static Yaw fromOrientationIndex(int orientationIndex) {
            return values()[orientationIndex & 3];
        }

        public static Yaw fromEntity(Entity entity) {
            int orientationIndex = (int)Math.floor((-entity.rotationYaw * 4.0F / 360.0F) + 2.5F);
            return fromOrientationIndex(orientationIndex);
        }

        Yaw(String name) {
            this.name = name;
        }

        public int getOrientationIndex() {
            return this.ordinal();
        }

        public int getRotationDegrees() {
            return this.getOrientationIndex() * 90;
        }

        public Yaw getOpposite() {
            return fromOrientationIndex(this.getOrientationIndex() + 2);
        }

        @Override
        public String toString() {
            return this.name;
        }

        @Override
        public String getName() {
            return this.name;
        }
    }

    public enum Pitch implements IStringSerializable {
        FORWARD("forward"),
        DOWNWARD("downward"),
        BACKWARD("backward"),
        UPWARD("upward");

        private final String name;

        public static Pitch fromOrientationIndex(int orientationIndex) {
            return values()[orientationIndex & 3];
        }

        public static Pitch fromEntity(Entity entity) {
            int orientationIndex = (int)Math.floor((-entity.rotationPitch * 4.0F / 360.0F) + 2.5F);
            return fromOrientationIndex(orientationIndex);
        }

        Pitch(String name) {
            this.name = name;
        }

        public int getOrientationIndex() {
            return this.ordinal();
        }

        public int getRotationDegrees() {
            return this.getOrientationIndex() * 90;
        }

        public Pitch getOpposite() {
            return fromOrientationIndex(this.getOrientationIndex() + 2);
        }

        @Override
        public String toString() {
            return this.name;
        }

        @Override
        public String getName() {
            return this.name;
        }
    }
}
