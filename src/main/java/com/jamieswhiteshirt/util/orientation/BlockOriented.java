package com.jamieswhiteshirt.util.orientation;

import net.minecraft.block.Block;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public abstract class BlockOriented extends Block {
    public static final PropertyOrientation ORIENTATION = PropertyOrientation.create("orientation");

    public BlockOriented(Material blockMaterialIn, MapColor blockMapColorIn) {
        super(blockMaterialIn, blockMapColorIn);
        this.setDefaultState(this.withOrientation(EnumOrientation.NORTH_FORWARD));
    }

    public BlockOriented(Material materialIn) {
        super(materialIn);
        this.setDefaultState(this.withOrientation(EnumOrientation.NORTH_FORWARD));
    }

    public IBlockState withOrientation(EnumOrientation orientation) {
        return this.getDefaultState().withProperty(ORIENTATION, orientation);
    }

    public IBlockState withYawPitch(EnumOrientation.Yaw yaw, EnumOrientation.Pitch pitch) {
        return this.withOrientation(EnumOrientation.fromYawPitch(yaw, pitch));
    }

    public IBlockState withPlacer(EntityLivingBase placer) {
        return this.withOrientation(EnumOrientation.fromEntity(placer).getOpposite());
    }

    @Override
    public void onBlockPlacedBy(World world, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack) {
        world.setBlockState(pos, withPlacer(placer), 2);
    }

    @Override
    public IBlockState onBlockPlaced(World world, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer) {
        return withPlacer(placer);
    }

    @Override
    @SuppressWarnings("deprecation")
    public IBlockState getStateFromMeta(int meta) {
        return this.withOrientation(EnumOrientation.fromOrientationIndex(meta));
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        return state.getValue(ORIENTATION).getOrientationIndex();
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer(this, ORIENTATION);
    }
}
