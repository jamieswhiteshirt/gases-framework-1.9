package com.jamieswhiteshirt.util.orientation;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import net.minecraft.block.properties.PropertyEnum;

import java.util.Collection;

public class PropertyOrientation extends PropertyEnum<EnumOrientation> {
    protected PropertyOrientation(String name, Collection<EnumOrientation> values)
    {
        super(name, EnumOrientation.class, values);
    }

    /**
     * Create a new PropertyOrientation with the given name
     */
    public static PropertyOrientation create(String name)
    {
        return create(name, Predicates.<EnumOrientation>alwaysTrue());
    }

    /**
     * Create a new PropertyOrientation with all orientations that match the given Predicate
     */
    public static PropertyOrientation create(String name, Predicate<EnumOrientation> filter)
    {
        return create(name, Collections2.filter(Lists.newArrayList(EnumOrientation.values()), filter));
    }

    /**
     * Create a new PropertyOrientation for the given orientation values
     */
    public static PropertyOrientation create(String name, Collection<EnumOrientation> values)
    {
        return new PropertyOrientation(name, values);
    }
}
