package com.jamieswhiteshirt.util.orientation;

import net.minecraft.util.IStringSerializable;

public enum EnumLocalFacing implements IStringSerializable {
    TOP("top"),
    BOTTOM("bottom"),
    FRONT("front"),
    BACK("back"),
    RIGHT("right"),
    LEFT("left");

    private final String name;

    EnumLocalFacing(String name) {
        this.name = name;
    }

    public int getIndex() {
        return this.ordinal();
    }

    @Override
    public String toString() {
        return this.name;
    }

    @Override
    public String getName() {
        return this.name;
    }
}
