package com.jamieswhiteshirt.util.configuration;

import com.jamieswhiteshirt.util.configuration.aggregator.IAggregator;
import net.minecraftforge.common.config.Configuration;

import javax.annotation.Nullable;
import java.lang.reflect.Field;
import java.util.Map;

public class ConfigurationContext {
    private final ReflectionConfiguration reflectionConfiguration;
    public final Configuration forgeConfiguration;
    private final Map<String, IAggregator> aggregatorMap;

    public ConfigurationContext(ReflectionConfiguration reflectionConfiguration, Configuration forgeConfiguration,
                                Map<String, IAggregator> aggregatorMap) {
        this.reflectionConfiguration = reflectionConfiguration;
        this.forgeConfiguration = forgeConfiguration;
        this.aggregatorMap = aggregatorMap;
    }

    /**
     * Initialize all Properties in a container in a forge configuration category
     * @param category the category the properties are initialized under
     * @param container the container of the Properties
     */
    public void initializeProperties(String category, Object container) {
        Class<?> clazz = container.getClass();
        for (Field field : clazz.getFields()) {
            Property property = field.getAnnotation(Property.class);
            if (property != null) {
                IAggregator aggregator = this.getAggregator(property.type());
                if (aggregator == null) {
                    // This is an error with the configuration structure, not the configuration content
                    throw new ConfigurationStructureError("No aggregator found for type " + property.type());
                }

                String name = field.getName();

                try {
                    field.set(container, aggregator.get(this, category, name, field.get(container),
                            property.comment()));
                } catch (IllegalAccessException e) {
                    // This is an error with the configuration structure, not the configuration content
                    throw new ConfigurationStructureError("Cannot access field " + clazz.getName() + " " + name, e);
                }
            }
        }
    }

    @Nullable
    public IAggregator getAggregator(String type) {
        return this.aggregatorMap.get(type);
    }
}
