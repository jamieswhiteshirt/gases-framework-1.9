package com.jamieswhiteshirt.util.configuration;

import com.jamieswhiteshirt.util.configuration.aggregator.category.StaticAggregator;
import com.jamieswhiteshirt.util.configuration.aggregator.IAggregator;
import com.jamieswhiteshirt.util.configuration.aggregator.property.BooleanAggregator;
import com.jamieswhiteshirt.util.configuration.aggregator.property.FloatAggregator;
import com.jamieswhiteshirt.util.configuration.aggregator.property.IntAggregator;
import com.jamieswhiteshirt.util.configuration.aggregator.property.StringAggregator;
import net.minecraftforge.common.config.Configuration;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public abstract class ReflectionConfiguration {
    @Property(comment = "Settings for the parsing of this configuration file", type = "category")
    public Meta meta = new Meta();

    public static class Meta {
        @Property(
                comment = "If true, errors in this configuration file will cause the game to crash.\n" +
                        "If false, errors in this configuration file will be handled and logged.\n" +
                        "This should only be enabled for debugging purposes.",
                type = "boolean"
        )
        public boolean failFast = false;
    }

    private final Logger logger;

    /**
     * Create a new ReflectionConfiguration with a logger.
     * @param logger
     */
    public ReflectionConfiguration(Logger logger) {
        this.logger = logger;
    }

    /**
     * Blame the content of the configuration for an error.
     * If failFast is true, a ConfigurationContentException is thrown and the game will crash.
     * If failFast is false, an error will be posted to the logger.
     * @param error the error string
     */
    public void blame(String error, Object... formatParams) {
        if (this.meta.failFast) {
            throw new ConfigurationContentException(String.format(error, formatParams));
        }
        else {
            logger.error("Configuration error: " + error, formatParams);
        }
    }

    /**
     * Get a map that binds Property types to aggregators.
     * @return a map that minds Property types to aggregators
     */
    protected Map<String, IAggregator> getAggregators() {
        Map<String, IAggregator> aggregatorMap = new HashMap<String, IAggregator>();

        aggregatorMap.put("category", new StaticAggregator());
        aggregatorMap.put("boolean", new BooleanAggregator());
        aggregatorMap.put("int", new IntAggregator());
        aggregatorMap.put("float", new FloatAggregator());
        aggregatorMap.put("string", new StringAggregator());

        return aggregatorMap;
    }

    /**
     * Initialize the ReflectionConfiguration from a configuration file.
     * @param configurationFile the configuration file
     */
    public void initialize(File configurationFile) {
        Configuration forgeConfiguration = new Configuration(configurationFile);
        new ConfigurationContext(this, forgeConfiguration, getAggregators()).initializeProperties("", this);
        if (forgeConfiguration.hasChanged()) {
            forgeConfiguration.save();
        }
    }
}
