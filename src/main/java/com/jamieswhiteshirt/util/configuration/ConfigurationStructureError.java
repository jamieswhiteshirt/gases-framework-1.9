package com.jamieswhiteshirt.util.configuration;

public class ConfigurationStructureError extends Error {
    public ConfigurationStructureError(String message) {
        super(message);
    }

    public ConfigurationStructureError(String message, Throwable cause) {
        super(message, cause);
    }
}
