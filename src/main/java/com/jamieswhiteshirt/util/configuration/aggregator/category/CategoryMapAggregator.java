package com.jamieswhiteshirt.util.configuration.aggregator.category;

import com.jamieswhiteshirt.util.configuration.ConfigurationContext;
import com.jamieswhiteshirt.util.configuration.aggregator.ICategoryAggregator;
import net.minecraftforge.common.config.ConfigCategory;

import java.util.HashMap;
import java.util.Map;

public abstract class CategoryMapAggregator extends AbstractMapAggregator {
    private final ICategoryAggregator categoryAggregator;

    public CategoryMapAggregator(ICategoryAggregator categoryAggregator) {
        super(categoryAggregator);
        this.categoryAggregator = categoryAggregator;
    }

    @Override
    protected Map<String, Object> read(ConfigurationContext context, String thisCategory,
                                       ConfigCategory thisConfigCategory) {
        Map<String, Object> categoryValues = new HashMap<String, Object>();

        for (ConfigCategory subConfigCategory : thisConfigCategory.getChildren()) {
            String key = subConfigCategory.getName().substring(thisConfigCategory.size());
            categoryValues.put(key, categoryAggregator.get(context, thisCategory, key, createDefaultValue(), null));
        }

        return categoryValues;
    }
}
