package com.jamieswhiteshirt.util.configuration.aggregator.property;

import com.jamieswhiteshirt.util.configuration.ConfigurationContext;

import javax.annotation.Nullable;

public class FloatAggregator extends PropertyAggregator {
    @Override
    public Object get(ConfigurationContext context, String category, String name, Object defaultValue,
                      @Nullable String comment) {
        return (float)context.forgeConfiguration.get(category, name, (Float)defaultValue, comment).getDouble();
    }
}
