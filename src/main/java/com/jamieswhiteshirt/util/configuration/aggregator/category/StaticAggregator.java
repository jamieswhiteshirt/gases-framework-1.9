package com.jamieswhiteshirt.util.configuration.aggregator.category;

import com.jamieswhiteshirt.util.configuration.ConfigurationContext;
import com.jamieswhiteshirt.util.configuration.ReflectionConfiguration;

import javax.annotation.Nullable;

public class StaticAggregator extends CategoryAggregator {
    @Override
    public Object get(ConfigurationContext context, String category, String name, Object defaultValue,
                      @Nullable String comment) {
        String thisCategory = this.getSubCategory(category, name);
        if (comment != null) {
            context.forgeConfiguration.getCategory(thisCategory).setComment(comment);
        }

        context.initializeProperties(thisCategory, defaultValue);
        return defaultValue;
    }
}
