package com.jamieswhiteshirt.util.configuration.aggregator;

import com.jamieswhiteshirt.util.configuration.ConfigurationContext;

import javax.annotation.Nullable;

public interface IAggregator {
    boolean exists(ConfigurationContext context, String category, String name);

    Object get(ConfigurationContext context, String category, String name, Object defaultValue,
               @Nullable String comment);
}
