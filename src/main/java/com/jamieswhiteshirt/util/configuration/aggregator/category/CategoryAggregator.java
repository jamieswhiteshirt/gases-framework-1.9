package com.jamieswhiteshirt.util.configuration.aggregator.category;

import com.jamieswhiteshirt.util.configuration.ConfigurationContext;
import com.jamieswhiteshirt.util.configuration.aggregator.ICategoryAggregator;
import net.minecraftforge.common.config.Configuration;

public abstract class CategoryAggregator implements ICategoryAggregator {
    protected String getSubCategory(String category, String name) {
        return category.length() > 0 ? (category + Configuration.CATEGORY_SPLITTER + name) : name;
    }

    @Override
    public boolean exists(ConfigurationContext context, String category, String name) {
        String thisCategory = getSubCategory(category, name);
        return context.forgeConfiguration.hasCategory(thisCategory);
    }
}
