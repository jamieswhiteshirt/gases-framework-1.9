package com.jamieswhiteshirt.util.configuration.aggregator.property;

import com.jamieswhiteshirt.util.configuration.ConfigurationContext;
import com.jamieswhiteshirt.util.configuration.aggregator.IPropertyAggregator;

public abstract class PropertyAggregator implements IPropertyAggregator {
    @Override
    public boolean exists(ConfigurationContext context, String category, String name) {
        return context.forgeConfiguration.hasKey(category, name);
    }
}
