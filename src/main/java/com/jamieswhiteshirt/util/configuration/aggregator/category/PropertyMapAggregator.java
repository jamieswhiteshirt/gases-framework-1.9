package com.jamieswhiteshirt.util.configuration.aggregator.category;

import com.jamieswhiteshirt.util.configuration.ConfigurationContext;
import com.jamieswhiteshirt.util.configuration.aggregator.IPropertyAggregator;
import net.minecraftforge.common.config.ConfigCategory;

import java.util.HashMap;
import java.util.Map;

public abstract class PropertyMapAggregator extends AbstractMapAggregator {
    private final IPropertyAggregator propertyAggregator;

    public PropertyMapAggregator(IPropertyAggregator propertyAggregator) {
        super(propertyAggregator);
        this.propertyAggregator = propertyAggregator;
    }

    @Override
    protected Map<String, Object> read(ConfigurationContext context, String thisCategory,
                                       ConfigCategory thisConfigCategory) {
        Map<String, Object> categoryValues = new HashMap<String, Object>();

        for (String key : thisConfigCategory.getValues().keySet()) {
            categoryValues.put(key, propertyAggregator.get(context, thisCategory, key, createDefaultValue(), null));
        }

        return categoryValues;
    }
}
