package com.jamieswhiteshirt.util.configuration.aggregator.category;

import com.jamieswhiteshirt.util.configuration.ConfigurationContext;
import com.jamieswhiteshirt.util.configuration.aggregator.IAggregator;
import net.minecraftforge.common.config.ConfigCategory;

import javax.annotation.Nullable;
import java.util.Map;

public abstract class AbstractMapAggregator extends CategoryAggregator {
    private final IAggregator aggregator;

    public AbstractMapAggregator(IAggregator aggregator) {
        this.aggregator = aggregator;
    }

    @Override
    public Object get(ConfigurationContext context, String category, String name, Object defaultValue,
                      @Nullable String comment) {
        String thisCategory = this.getSubCategory(category, name);

        boolean hasThisCategory = this.exists(context, category, name);
        ConfigCategory thisConfigCategory = context.forgeConfiguration.getCategory(thisCategory);

        if (comment != null) {
            thisConfigCategory.setComment(comment);
        }

        if (hasThisCategory) {
            return this.read(context, thisCategory, thisConfigCategory);
        }
        else {
            @SuppressWarnings("unchecked")
            Map<String, Object> defaultValues = (Map<String, Object>)defaultValue;
            this.writeDefaultValues(context, thisCategory, defaultValues);
            return defaultValues;
        }
    }

    protected void writeDefaultValues(ConfigurationContext context, String thisCategory,
                                      Map<String, Object> defaultValues) {
        for (Map.Entry<String, Object> entry : defaultValues.entrySet()) {
            aggregator.get(context, thisCategory, entry.getKey(), entry.getValue(), null);
        }
    }

    protected abstract Map<String, Object> read(ConfigurationContext context, String thisCategory,
                                       ConfigCategory thisConfigCategory);

    protected abstract Object createDefaultValue();
}
