package com.jamieswhiteshirt.util.configuration;

public class ConfigurationContentException extends RuntimeException {
    public ConfigurationContentException(String error) {
        super(error);
    }
}
