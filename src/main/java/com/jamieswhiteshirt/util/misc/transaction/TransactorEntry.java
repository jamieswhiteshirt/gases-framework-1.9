package com.jamieswhiteshirt.util.misc.transaction;

public class TransactorEntry {
    public final Account account;
    public final int rate;

    public TransactorEntry(Account account, int rate) {
        this.account = account;
        this.rate = rate;
    }
}
