package com.jamieswhiteshirt.util.misc.transaction;

import javax.annotation.Nullable;

public class Transactor {
    private final TransactorEntry[] from;
    private final TransactorEntry[] to;

    public Transactor(TransactorEntry[] from, TransactorEntry[] to) {
        this.from = from;
        this.to = to;
    }

    /**
     * Returns the largest transaction that can be completed
     * @return the largest transaction that can be completed
     */
    public int getCompleteCapacity() {
        @Nullable
        Integer capacity = null;
        for (TransactorEntry entry : this.from) {
            int i = entry.account.getSubtractCapacity() / entry.rate;
            if (capacity == null || i < capacity) {
                capacity = i;
            }
        }
        for (TransactorEntry entry : this.to) {
            int i = entry.account.getAddCapacity() / entry.rate;
            if (capacity == null || i < capacity) {
                capacity = i;
            }
        }
        return capacity != null ? capacity : 0;
    }

    /**
     * Returns true if the entire transaction can be completed.
     * @param transaction the transaction to complete
     * @return true if the entire transaction can be completed
     */
    public boolean canCompleteAll(int transaction) {
        for (TransactorEntry entry : this.from) {
            if (!entry.account.canSubtractAll(transaction * entry.rate)) {
                return false;
            }
        }
        for (TransactorEntry entry : this.to) {
            if (!entry.account.canAddAll(transaction * entry.rate)) {
                return false;
            }
        }

        return from.length > 0 || to.length > 0;
    }

    /**
     * Try completing the transaction, all or nothing. Returns the completed transaction.
     * @param transaction the transaction to complete
     * @return the completed transaction
     */
    public int tryCompleteAll(int transaction) {
        if (canCompleteAll(transaction)) {
            for (TransactorEntry entry : this.from) {
                entry.account.unsafeSubtract(transaction * entry.rate);
            }
            for (TransactorEntry entry : this.to) {
                entry.account.unsafeAdd(transaction * entry.rate);
            }
            return transaction;
        }
        else {
            return 0;
        }
    }

    /**
     * Complete as much of the transaction as possible. Returns the completed transaction.
     * @param transaction the transaction to complete
     * @return the completed transaction
     */
    public int completeAtMost(int transaction) {
        int result = Math.min(transaction, getCompleteCapacity());
        for (TransactorEntry entry : this.from) {
            entry.account.unsafeSubtract(result * entry.rate);
        }
        for (TransactorEntry entry : this.to) {
            entry.account.unsafeAdd(result * entry.rate);
        }
        return result;
    }
}
