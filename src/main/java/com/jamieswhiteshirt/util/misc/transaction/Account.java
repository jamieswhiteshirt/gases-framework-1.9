package com.jamieswhiteshirt.util.misc.transaction;

public class Account {
    private int amount;
    private int maxAmount;

    public Account(int amount, int maxAmount) {
        this.amount = amount;
        this.maxAmount = maxAmount;
    }

    public void reset(int amount, int maxAmount) {
        this.amount = amount;
        this.maxAmount = maxAmount;
    }

    /**
     * Get the amount.
     * @return the amount
     */
    public int getAmount() {
        return this.amount;
    }

    /**
     * Set the amount. The amount is clamped below the max amount.
     * @param amount the amount
     */
    public void setAmount(int amount) {
        if (amount > this.maxAmount) {
            this.amount = this.maxAmount;
        }
        else {
            this.amount = amount;
        }
    }

    /**
     * Set the amount regardless of state.
     * @param amount the amount
     */
    public void unsafeSetAmount(int amount) {
        this.amount = amount;
    }

    /**
     * Get the max amount.
     * @return the max amount
     */
    public int getMaxAmount() {
        return this.maxAmount;
    }

    /**
     * Set the max amount. The amount is clamped below the max amount.
     * @param maxAmount the max amount
     */
    public void setMaxAmount(int maxAmount) {
        this.maxAmount = maxAmount;
        if (this.amount > maxAmount) {
            this.amount = maxAmount;
        }
    }

    /**
     * Set the max amount regardless of state.
     * @param maxAmount the max amount
     */
    public void unsafeSetMaxAmount(int maxAmount) {
        this.maxAmount = maxAmount;
    }

    /**
     * Returns the largest amount that can be added.
     * @return the largest amount that can be added
     */
    public int getAddCapacity() {
        return this.maxAmount - this.amount;
    }

    /**
     * Returns true if the entire amount can be added.
     * @param amount the amount to add
     * @return true if the entire amount can be added
     */
    public boolean canAddAll(int amount) {
        return amount <= getAddCapacity();
    }

    /**
     * Try adding the amount, all or nothing. Returns the added amount.
     * @param amount the amount to add
     * @return the added amount
     */
    public int tryAddAll(int amount) {
        if (this.canAddAll(amount)) {
            this.amount += amount;
            return amount;
        }
        else {
            return 0;
        }
    }

    /**
     * Add as much of the amount as possible. Returns the added amount.
     * @param amount the amount to add at most
     * @return the added amount
     */
    public int addAtMost(int amount) {
        int result = this.canAddAll(amount) ? amount : this.maxAmount - this.amount;
        this.amount += result;
        return result;
    }

    /**
     * Add the entire amount regardless of state.
     * @param amount the amount to add
     */
    public void unsafeAdd(int amount) {
        this.amount += amount;
    }

    /**
     * Returns the largest amount that can be subtracted.
     * @return the largest amount that can be subtracted
     */
    public int getSubtractCapacity() {
        return this.amount;
    }

    /**
     * Returns true if the entire amount can be subtracted.
     * @param amount the amount to subtract
     * @return true if the entire amount can be subtracted
     */
    public boolean canSubtractAll(int amount) {
        return amount <= this.amount;
    }

    /**
     * Try subtracting the amount, all or nothing. Returns the subtracted amount.
     * @param amount the amount to subtract
     * @return the subtracted amount
     */
    public int trySubtractAll(int amount) {
        if (this.canSubtractAll(amount)) {
            this.amount -= amount;
            return amount;
        }
        else {
            return 0;
        }
    }

    /**
     * Subtract as much of the amount as possible. Returns the subtracted amount.
     * @param amount the amount to subtract at most
     * @return the subtracted amount
     */
    public int subtractAtMost(int amount) {
        int result = this.canSubtractAll(amount) ? amount : this.amount;
        this.amount -= result;
        return result;
    }

    /**
     * Subtract the entire amount regardless of state.
     * @param amount the amount to subtract
     */
    public void unsafeSubtract(int amount) {
        this.amount -= amount;
    }

}
