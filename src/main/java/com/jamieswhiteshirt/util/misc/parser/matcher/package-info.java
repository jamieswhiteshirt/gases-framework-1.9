@ParametersAreNonnullByDefault
@MethodsReturnNonnullByDefault
package com.jamieswhiteshirt.util.misc.parser.matcher;

import mcp.MethodsReturnNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;