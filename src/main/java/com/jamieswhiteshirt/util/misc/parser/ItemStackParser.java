package com.jamieswhiteshirt.util.misc.parser;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemStackParser extends SplitParserBase<ItemStack> {
    public static final ItemStackParser INSTANCE = new ItemStackParser();

    private ItemStackParser() {

    }

    @Override
    public ItemStack parse(String[] values) throws ParserException {
        Item item = ItemParser.INSTANCE.parse(values[0]);
        int amount = values.length > 1 ? IntParser.INSTANCE.parse(values[1]) : 1;
        int meta = values.length > 2 ? IntParser.INSTANCE.parse(values[2]) : 0;

        return new ItemStack(item, amount, meta);
    }
}
