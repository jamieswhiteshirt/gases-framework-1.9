package com.jamieswhiteshirt.util.misc.parser;

public abstract class ParserBase<T> {
    public abstract T parse(String value) throws ParserException;
}
