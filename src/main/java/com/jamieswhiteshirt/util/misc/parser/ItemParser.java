package com.jamieswhiteshirt.util.misc.parser;

import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;

public class ItemParser extends ParserBase<Item> {
    public static final ItemParser INSTANCE = new ItemParser();

    private ItemParser() {

    }

    @Override
    public Item parse(String value) throws ParserException {
        Item item = Item.REGISTRY.getObject(new ResourceLocation(value));
        if (item == null) {
            throw new ParserException("Unknown item " + value);
        }
        else {
            return item;
        }
    }
}
