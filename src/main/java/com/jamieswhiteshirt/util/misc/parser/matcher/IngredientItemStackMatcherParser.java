package com.jamieswhiteshirt.util.misc.parser.matcher;

import com.jamieswhiteshirt.util.misc.matcher.IMatcher;
import com.jamieswhiteshirt.util.misc.matcher.IngredientItemStackMatcher;
import com.jamieswhiteshirt.util.misc.matcher.IntMatcher;
import com.jamieswhiteshirt.util.misc.parser.IntParser;
import com.jamieswhiteshirt.util.misc.parser.ItemParser;
import com.jamieswhiteshirt.util.misc.parser.ParserException;
import com.jamieswhiteshirt.util.misc.parser.SplitParserBase;
import net.minecraft.item.Item;

public class IngredientItemStackMatcherParser extends SplitParserBase<IngredientItemStackMatcher> {
    public static final IngredientItemStackMatcherParser INSTANCE = new IngredientItemStackMatcherParser();

    private IngredientItemStackMatcherParser() {

    }

    @Override
    public IngredientItemStackMatcher parse(String[] values) throws ParserException {
        Item item = ItemParser.INSTANCE.parse(values[0]);
        int amount = values.length > 1 ? IntParser.INSTANCE.parse(values[1]) : 1;
        IMatcher<Integer> meta = values.length > 2 ? IntMatcherParser.INSTANCE.parse(values[2]) :
                IntMatcher.Any.INSTANCE;

        return new IngredientItemStackMatcher(item, amount, meta);
    }
}
