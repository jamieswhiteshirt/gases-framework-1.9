package com.jamieswhiteshirt.util.misc.parser;

import java.util.Arrays;

public class ParserUtil {
    public static String[] copyAtMost(String[] values, int length) {
        return Arrays.copyOf(values, Math.min(length, values.length));
    }
}
