package com.jamieswhiteshirt.util.misc.parser.matcher;

import com.jamieswhiteshirt.util.misc.matcher.IMatcher;
import com.jamieswhiteshirt.util.misc.matcher.IntMatcher;
import com.jamieswhiteshirt.util.misc.matcher.ItemStackMatcher;
import com.jamieswhiteshirt.util.misc.parser.ItemParser;
import com.jamieswhiteshirt.util.misc.parser.ParserException;
import com.jamieswhiteshirt.util.misc.parser.SplitParserBase;
import net.minecraft.item.Item;

public class ItemStackMatcherParser extends SplitParserBase<ItemStackMatcher> {
    public static final ItemStackMatcherParser INSTANCE = new ItemStackMatcherParser();

    private ItemStackMatcherParser() {

    }

    @Override
    public ItemStackMatcher parse(String[] values) throws ParserException {
        Item item = ItemParser.INSTANCE.parse(values[0]);
        IMatcher<Integer> amount = values.length > 1 ? IntMatcherParser.INSTANCE.parse(values[1]) : IntMatcher.Any.INSTANCE;
        IMatcher<Integer> meta = values.length > 2 ? IntMatcherParser.INSTANCE.parse(values[2]) : IntMatcher.Any.INSTANCE;

        return new ItemStackMatcher(item, amount, meta);
    }
}
