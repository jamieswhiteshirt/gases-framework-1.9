package com.jamieswhiteshirt.util.misc.parser;

public class IntParser extends ParserBase<Integer> {
    public static final IntParser INSTANCE = new IntParser();

    private IntParser() {

    }

    @Override
    public Integer parse(String value) throws ParserException {
        try {
            return Integer.parseInt(value);
        }
        catch (NumberFormatException e) {
            throw new ParserException(e);
        }
    }
}
