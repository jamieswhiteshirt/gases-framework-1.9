package com.jamieswhiteshirt.util.misc.parser.matcher;

import com.jamieswhiteshirt.util.misc.matcher.IMatcher;
import com.jamieswhiteshirt.util.misc.matcher.IntMatcher;
import com.jamieswhiteshirt.util.misc.parser.IntParser;
import com.jamieswhiteshirt.util.misc.parser.ParserBase;
import com.jamieswhiteshirt.util.misc.parser.ParserException;

public class IntMatcherParser extends ParserBase<IMatcher<Integer>> {
    public static final IntMatcherParser INSTANCE = new IntMatcherParser();

    private IntMatcherParser() {

    }

    @Override
    public IMatcher<Integer> parse(String value) throws ParserException {
        if (value.equals("*")) {
            return IntMatcher.Any.INSTANCE;
        }
        else if (value.contains("~")) {
            String[] values = value.split("~");
            if (values.length != 2) {
                throw new ParserException("Invalid integer range");
            }
            String a = values[0];
            String b = values[1];
            if (a.length() > 0) {
                if (b.length() > 0) {
                    return new IntMatcher.InRange(IntParser.INSTANCE.parse(a), IntParser.INSTANCE.parse(b));
                }
                else {
                    return new IntMatcher.MoreThanOrEqual(IntParser.INSTANCE.parse(a));
                }
            }
            else {
                if (b.length() > 0) {
                    return new IntMatcher.LessThanOrEqual(IntParser.INSTANCE.parse(b));
                }
                else {
                    throw new ParserException("Invalid integer range");
                }
            }
        }
        else {
            return new IntMatcher.EqualTo(IntParser.INSTANCE.parse(value));
        }
    }
}
