package com.jamieswhiteshirt.util.misc.parser;

public abstract class SplitParserBase<T> extends ParserBase<T> {
    @Override
    public T parse(String value) throws ParserException {
        return parse(value.split(" "));
    }

    public abstract T parse(String[] values) throws ParserException;
}
