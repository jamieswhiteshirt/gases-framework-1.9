package com.jamieswhiteshirt.util.misc.matcher;

public abstract class IntMatcher implements IMatcher<Integer> {
    public static class Any extends IntMatcher {
        public static final Any INSTANCE = new Any();

        private Any() {

        }

        @Override
        public boolean match(Integer value) {
            return true;
        }

        @Override
        public String toString() {
            return "*";
        }
    }

    private abstract static class Simple extends IntMatcher {
        public final int value;

        public Simple(int value) {
            this.value = value;
        }
    }

    public static class EqualTo extends Simple {
        public EqualTo(int value) {
            super(value);
        }

        @Override
        public boolean match(Integer value) {
            return value == this.value;
        }

        @Override
        public String toString() {
            return Integer.toString(value);
        }
    }

    public static class LessThanOrEqual extends Simple {
        public LessThanOrEqual(int value) {
            super(value);
        }

        @Override
        public boolean match(Integer value) {
            return value <= this.value;
        }

        @Override
        public String toString() {
            return "<=" + this.value;
        }
    }

    public static class MoreThanOrEqual extends Simple {
        public MoreThanOrEqual(int value) {
            super(value);
        }

        @Override
        public boolean match(Integer value) {
            return value >= this.value;
        }

        @Override
        public String toString() {
            return ">=" + this.value;
        }
    }

    public static class InRange extends IntMatcher {
        public final int least;
        public final int most;

        public InRange(int a, int b) {
            this.least = Math.min(a, b);
            this.most = Math.max(a, b);
        }

        @Override
        public boolean match(Integer value) {
            return value >= this.least && value <= this.most;
        }
    }
}
