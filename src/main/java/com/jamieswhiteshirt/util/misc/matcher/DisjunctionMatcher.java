package com.jamieswhiteshirt.util.misc.matcher;

import java.util.Collection;

public class DisjunctionMatcher<T> extends CompositeMatcher<T> {
    public DisjunctionMatcher(Collection<IMatcher<T>> matchers) {
        super(matchers);
    }

    @Override
    public boolean match(T value) {
        for (IMatcher<T> matcher : matchers) {
            if (!matcher.match(value)) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected char getSeparator() {
        return '|';
    }
}
