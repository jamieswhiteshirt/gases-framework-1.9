package com.jamieswhiteshirt.util.misc.matcher;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class IngredientItemStackMatcher implements Comparable<IngredientItemStackMatcher>, IMatcher<ItemStack> {
    public final Item item;
    public final int stackSize;
    public final IMatcher<Integer> itemDamage;

    public IngredientItemStackMatcher(Item item, int stackSize, IMatcher<Integer> itemDamage) {
        this.item = item;
        this.stackSize = stackSize;
        this.itemDamage = itemDamage;
    }

    @Override
    public boolean match(ItemStack value) {
        return this.item == value.getItem() && this.stackSize <= value.stackSize &&
                this.itemDamage.match(value.getItemDamage());
    }

    @Override
    public String toString() {
        return this.item.getRegistryName() + " " + this.stackSize + " " + this.itemDamage;
    }

    @Override
    public int compareTo(IngredientItemStackMatcher o) {
        return this.stackSize - o.stackSize;
    }
}
