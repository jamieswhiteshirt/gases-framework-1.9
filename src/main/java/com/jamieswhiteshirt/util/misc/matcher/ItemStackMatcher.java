package com.jamieswhiteshirt.util.misc.matcher;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemStackMatcher implements IMatcher<ItemStack> {
    public final Item item;
    public final IMatcher<Integer> stackSize;
    public final IMatcher<Integer> meta;

    public ItemStackMatcher(Item item, IMatcher<Integer> stackSize, IMatcher<Integer> meta) {
        this.item = item;
        this.stackSize = stackSize;
        this.meta = meta;
    }

    @Override
    public boolean match(ItemStack value) {
        return this.item == value.getItem() && this.stackSize.match(value.stackSize) &&
                this.meta.match(value.getItemDamage());
    }

    @Override
    public String toString() {
        return this.item.getRegistryName() + " " + this.stackSize + " " + this.meta;
    }
}
