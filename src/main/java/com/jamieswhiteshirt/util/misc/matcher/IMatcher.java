package com.jamieswhiteshirt.util.misc.matcher;

public interface IMatcher<T> {
    boolean match(T value);
}
