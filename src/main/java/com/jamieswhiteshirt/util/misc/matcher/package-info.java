@ParametersAreNonnullByDefault
@MethodsReturnNonnullByDefault
package com.jamieswhiteshirt.util.misc.matcher;

import mcp.MethodsReturnNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;