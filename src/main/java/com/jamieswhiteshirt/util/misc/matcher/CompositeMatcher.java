package com.jamieswhiteshirt.util.misc.matcher;

import com.google.common.collect.ImmutableList;

import java.util.Collection;

public abstract class CompositeMatcher<T> implements IMatcher<T> {
    public final ImmutableList<IMatcher<T>> matchers;

    public CompositeMatcher(Collection<IMatcher<T>> matchers) {
        this.matchers = ImmutableList.copyOf(matchers);
    }

    @Override
    public String toString() {
        String separator = "";
        StringBuilder builder = new StringBuilder();
        for (IMatcher<T> matcher : matchers) {
            builder.append(separator).append(matcher);
            separator = "&";
        }
        return builder.toString();
    }

    protected abstract char getSeparator();
}
