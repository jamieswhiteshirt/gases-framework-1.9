package com.jamieswhiteshirt.util.misc.matcher;

import java.util.Collection;

public class ConjunctionMatcher<T> extends CompositeMatcher<T> {
    public ConjunctionMatcher(Collection<IMatcher<T>> matchers) {
        super(matchers);
    }

    @Override
    public boolean match(T value) {
        for (IMatcher<T> matcher : matchers) {
            if (!matcher.match(value)) {
                return false;
            }
        }
        return true;
    }

    @Override
    protected char getSeparator() {
        return '&';
    }
}
