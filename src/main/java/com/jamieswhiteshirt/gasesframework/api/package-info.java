/**
 *	The Gases Framework API, used to do whatever Glenn's Gases can do, and more.
 */
@API(apiVersion = GFAPI.VERSION, owner = GFAPI.OWNER, provides = GFAPI.PROVIDES)
@ParametersAreNonnullByDefault
@MethodsReturnNonnullByDefault
package com.jamieswhiteshirt.gasesframework.api;

import mcp.MethodsReturnNonnullByDefault;
import net.minecraftforge.fml.common.API;

import javax.annotation.ParametersAreNonnullByDefault;