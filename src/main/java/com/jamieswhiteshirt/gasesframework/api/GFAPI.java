package com.jamieswhiteshirt.gasesframework.api;

public class GFAPI {
    public static final String OWNER = "gasesframework";
    public static final String VERSION = "2.0.0";
    public static final String MCVERSION = "1.9.4";
    public static final String PROVIDES = "gasesFrameworkAPI";
}
