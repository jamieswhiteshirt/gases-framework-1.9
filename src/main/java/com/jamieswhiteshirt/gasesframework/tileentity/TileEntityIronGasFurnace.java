package com.jamieswhiteshirt.gasesframework.tileentity;

import com.jamieswhiteshirt.gasesframework.GasesFramework;
import com.jamieswhiteshirt.gasesframework.block.BlockGasFurnaceBase;
import com.jamieswhiteshirt.gasesframework.init.GFBlocks;
import net.minecraft.block.Block;

public class TileEntityIronGasFurnace extends TileEntityGasFurnaceBase {
    public TileEntityIronGasFurnace() {
        super(GasesFramework.configuration.blocks.ironGasFurnace);
    }
}
