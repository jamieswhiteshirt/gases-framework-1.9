package com.jamieswhiteshirt.gasesframework.tileentity;

import com.jamieswhiteshirt.gasesframework.GasesFramework;
import com.jamieswhiteshirt.gasesframework.block.BlockGasFurnaceBase;
import com.jamieswhiteshirt.gasesframework.item.crafting.GasFurnaceRecipes;
import com.jamieswhiteshirt.gasesframework.inventory.ContainerGasFurnace;
import com.jamieswhiteshirt.gasesframework.item.crafting.smelting.SmeltingRecipe;
import com.jamieswhiteshirt.util.misc.transaction.Account;
import com.jamieswhiteshirt.util.misc.transaction.Transactor;
import com.jamieswhiteshirt.util.misc.transaction.TransactorEntry;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntityLockable;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.wrapper.SidedInvWrapper;

import javax.annotation.Nullable;

public abstract class TileEntityGasFurnaceBase extends TileEntityLockable implements ITickable, ISidedInventory {
    public interface ISettings {
        int getEmissionInterval();

        int getMaxFuelUnits();

        int getTemperaturePerFuelUnit();

        int getTemperatureLostPerTick();

        int getMaxTemperature();
    }

    // Named slot indices
    public static final int SLOT_INPUT = 0;
    public static final int SLOT_OUTPUT = 1;

    // Named field indices (used by GUI)
    public static final int FIELD_MAX_FUEL_UNITS = 0;
    public static final int FIELD_FUEL_UNITS = 1;
    public static final int FIELD_MAX_TEMPERATURE = 2;
    public static final int FIELD_TEMPERATURE = 3;
    public static final int FIELD_MAX_COOK_TIME = 4;
    public static final int FIELD_COOK_TIME = 5;

    // Accessible slot indices by side
    private static final int[] SLOTS_TOP = {SLOT_INPUT};
    private static final int[] SLOTS_SIDES = {SLOT_INPUT};
    private static final int[] SLOTS_BOTTOM = {SLOT_OUTPUT};

    // Forge Capability handlers for item IO
    private final IItemHandler handlerTop = new SidedInvWrapper(this, EnumFacing.UP);
    private final IItemHandler handlerBottom = new SidedInvWrapper(this, EnumFacing.DOWN);
    private final IItemHandler handlerSide = new SidedInvWrapper(this, EnumFacing.WEST);

    // Settings object from the configurations
    private final ISettings settings;

    // Server/inventory state
    private final ItemStack[] stacks = new ItemStack[2];
    private final Account fuelUnits;
    private final Account temperature;
    private final Account emissionAccumulator;
    private final Account cookTime;
    private String customName;

    // Shared state. Must be synchronized manually
    private int temperatureStage = 0;
    private boolean temperatureStageChanging = false;
    private boolean burning = false;
    private boolean burningChanging = false;

    // Transactor used to convert fuel into temperature and emissions
    private final Transactor burnFuel;

    public TileEntityGasFurnaceBase(ISettings settings) {
        this.settings = settings;
        this.fuelUnits = new Account(this.settings.getMaxFuelUnits(), this.settings.getMaxFuelUnits());
        this.temperature = new Account(0, this.settings.getMaxTemperature());
        this.emissionAccumulator = new Account(0, this.settings.getEmissionInterval());
        this.cookTime = new Account(0, 0);

        this.burnFuel = new Transactor(new TransactorEntry[] {
                new TransactorEntry(this.fuelUnits, 1)
        }, new TransactorEntry[] {
                new TransactorEntry(this.temperature, this.settings.getTemperaturePerFuelUnit()),
                new TransactorEntry(this.emissionAccumulator, 1)
        });
    }

    /**
     * Get the burning state of this furnace.
     */
    public boolean getBurning() {
        return this.burning;
    }

    /**
     * Set the burning state of this furnace. If the burning state changes, the block is notified.
     */
    public void setBurning(boolean isBurning) {
        if (this.burning != isBurning) {
            this.burningChanging = true;
            this.burning = isBurning;
            BlockGasFurnaceBase block = this.getFurnaceBlock();
            if (block != null) {
                block.onIsBurningChange(this.worldObj, this.pos, this);
            }
            this.burningChanging = false;
        }
    }

    /**
     * Returns true if the burning state is currently changing.
     */
    public boolean isBurningChanging() {
        return this.burningChanging;
    }

    /**
     * Get the temperature stage of this furnace.
     */
    public int getTemperatureStage() {
        return this.temperatureStage;
    }

    /**
     * Set the temperature stage of this furnace. If the temperature stage changes, the block is notified.
     */
    public void setTemperatureStage(int temperatureStage) {
        if (this.temperatureStage != temperatureStage) {
            this.temperatureStageChanging = true;
            this.temperatureStage = temperatureStage;
            BlockGasFurnaceBase block = this.getFurnaceBlock();
            if (block != null) {
                block.onTemperatureStageChange(this.worldObj, this.pos, this);
            }
            this.temperatureStageChanging = false;
        }
    }

    /**
     * Returns true if the temperature stage is currently changing.
     */
    public boolean isTemperatureStageChanging() {
        return this.temperatureStageChanging;
    }

    /**
     * Get the furnace block if is is a BlockGasFurnaceBase.
     */
    @Nullable
    private BlockGasFurnaceBase getFurnaceBlock() {
        Block block = this.worldObj.getBlockState(this.pos).getBlock();
        if (block instanceof BlockGasFurnaceBase) {
            return (BlockGasFurnaceBase)block;
        }
        else {
            return null;
        }
    }

    /**
     * Returns true if the output slot can hold the output.
     */
    private boolean canCompleteSmelting(SmeltingRecipe recipe) {
        if (this.stacks[SLOT_OUTPUT] != null) {
            if (this.stacks[SLOT_OUTPUT].isItemEqual(recipe.outputStack)) {
                int stackSize = stacks[SLOT_OUTPUT].stackSize + recipe.outputStack.stackSize;
                return stackSize <= this.getInventoryStackLimit() &&
                        stackSize <= this.stacks[SLOT_OUTPUT].getMaxStackSize();
            }
            else {
                // item in output slot is not compatible
                return false;
            }
        }
        else {
            // no item in output slot
            return true;
        }
    }

    /**
     * Turn items from the input stack into the appropriate smelted item in the furnace output stack.
     */
    private void smeltItem(SmeltingRecipe recipe) {
        if (this.stacks[SLOT_OUTPUT] == null) {
            this.stacks[SLOT_OUTPUT] = recipe.outputStack.copy();
        }
        else if (this.stacks[SLOT_OUTPUT].getItem() == recipe.outputStack.getItem()) {
            this.stacks[SLOT_OUTPUT].stackSize += recipe.outputStack.stackSize;
        }

        this.stacks[SLOT_INPUT].stackSize -= recipe.inputStackSize;

        if (this.stacks[SLOT_INPUT].stackSize <= 0) {
            this.stacks[SLOT_INPUT] = null;
        }
    }

    /**
     * Get the smelting result for the item in the input slot, if any.
     */
    @Nullable
    private SmeltingRecipe getSmeltingRecipe() {
        if (this.stacks[SLOT_INPUT] != null) {
            return GasFurnaceRecipes.getSmeltingResult(this.stacks[SLOT_INPUT], this.temperature.getAmount());
        }
        else {
            return null;
        }
    }

    /**
     * Returns true if the furnace needs to emit smoke to continue using fuel.
     */
    private boolean isChoked() {
        return this.emissionAccumulator.getAddCapacity() == 0;
    }

    /**
     * Try to emit smoke to nullify the emission accumulator.
     */
    private boolean tryEmitSmoke() {
        if (this.isChoked()) {
            //TODO: Try to emit smoke
            this.emissionAccumulator.unsafeSubtract(this.settings.getEmissionInterval());
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Update the temperature of the gas furnace. The temperature falls by a certain amount per tick and increases by
     * consuming fuel.
     */
    private boolean updateTemperature() {
        // If the furnace state changes, this should be true
        boolean isDirty = false;
        if (this.temperature.subtractAtMost(this.settings.getTemperatureLostPerTick()) > 0) {
            isDirty = true;
        }

        if (this.burnFuel.completeAtMost(1) > 0) {
            isDirty = true;
        }

        if (this.tryEmitSmoke()) {
            isDirty = true;
        }

        return isDirty;
    }

    /**
     * Update the max amount of the cookTime account based on the currect active smelting parameters.
     */
    private void updateTotalCookTime() {
        SmeltingRecipe recipe = this.getSmeltingRecipe();
        if (recipe != null) {
            this.cookTime.setMaxAmount(recipe.smeltingTime * 1000);
        }
        else {
            this.cookTime.setMaxAmount(0);
        }
    }

    /**
     * Evaluate if the gas furnace is burning, e.g. is it not choked and has fuel.
     * Should only be used on server side.
     */
    private boolean evaluateIsBurning() {
        return !this.isChoked() && this.fuelUnits.getSubtractCapacity() > 0;
    }

    /**
     * Evaluate the current temperature stage based on temperature account.
     * Should only be used on server side.
     */
    private int evaluateTemperatureStage() {
        int temperature = this.temperature.getAmount();
        int maxTemperature = this.temperature.getMaxAmount();
        return Math.min(maxTemperature > 0 ? 4 * temperature / maxTemperature : 0, 3);
    }

    @Override
    public void update() {
        if (!this.worldObj.isRemote) {
            boolean isDirty = false;

            /*if (this.worldObj.getBlockState(this.pos.add(0, 1, 0)).getBlock() == GFBlocks.GAS_FURNACE_IRON_IDLE) {
                this.fuelUnits.addAtMost(10);
            }*/

            this.fuelUnits.addAtMost(10);

            if (this.updateTemperature()) {
                isDirty = true;
            }

            SmeltingRecipe recipe = this.getSmeltingRecipe();
            if (recipe != null && this.canCompleteSmelting(recipe)) {
                this.cookTime.addAtMost(this.temperature.getAmount());

                if (this.cookTime.getAddCapacity() == 0) {
                    this.cookTime.setAmount(0);
                    this.smeltItem(recipe);
                    this.updateTotalCookTime();

                    isDirty = true;
                }
            }
            else {
                this.cookTime.setAmount(0);
            }

            this.setBurning(this.evaluateIsBurning());
            this.setTemperatureStage(this.evaluateTemperatureStage());

            if (isDirty) {
                this.markDirty();
            }
        }
    }

    @Override
    @Nullable
    public SPacketUpdateTileEntity getUpdatePacket() {
        // Synchronize temperature stage to the client when the tile entity is received by the client
        return new SPacketUpdateTileEntity(this.pos, 0, this.getUpdateTag());
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        // Adds data necessary to bind the update packet to this tile entity
        NBTTagCompound tagCompound = super.getUpdateTag();
        tagCompound.setBoolean("IsBurning", this.getBurning());
        tagCompound.setInteger("TemperatureStage", this.getTemperatureStage());
        return tagCompound;
    }

    @Override
    public void onDataPacket(NetworkManager networkManager, SPacketUpdateTileEntity packet) {
        // Delegate packet to update tag handler
        this.handleUpdateTag(packet.getNbtCompound());
    }

    @Override
    public void handleUpdateTag(NBTTagCompound tagCompound) {
        // Apply temperature stage received from server
        this.setBurning(tagCompound.getBoolean("IsBurning"));
        this.setTemperatureStage(tagCompound.getInteger("TemperatureStage"));
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);

        NBTTagList items = compound.getTagList("Items", 10);
        for (int i = 0; i < items.tagCount(); ++i) {
            NBTTagCompound item = items.getCompoundTagAt(i);
            int j = item.getByte("Slot");

            if (j >= 0 && j < this.stacks.length) {
                this.stacks[j] = ItemStack.loadItemStackFromNBT(item);
            }
        }

        this.fuelUnits.setAmount(compound.getInteger("FuelUnits"));
        this.temperature.setAmount(compound.getInteger("Temperature"));
        this.emissionAccumulator.setAmount(compound.getInteger("EmissionAccumulator"));
        this.cookTime.reset(compound.getInteger("CookTime"), compound.getInteger("MaxCookTime"));

        if (compound.hasKey("CustomName", 8)) {
            this.customName = compound.getString("CustomName");
        }

        this.temperatureStage = compound.getInteger("TemperatureStage");
        this.burning = compound.getBoolean("Burning");
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);

        NBTTagList items = new NBTTagList();
        for (int i = 0; i < this.stacks.length; ++i) {
            if (this.stacks[i] != null) {
                NBTTagCompound nbttagcompound = new NBTTagCompound();
                nbttagcompound.setByte("Slot", (byte)i);
                this.stacks[i].writeToNBT(nbttagcompound);
                items.appendTag(nbttagcompound);
            }
        }

        compound.setTag("Items", items);
        compound.setInteger("FuelUnits", this.fuelUnits.getAmount());
        compound.setInteger("Temperature", this.temperature.getAmount());
        compound.setInteger("EmissionAccumulator", this.emissionAccumulator.getAmount());
        compound.setInteger("CookTime", this.cookTime.getAmount());
        compound.setInteger("MaxCookTime", this.cookTime.getMaxAmount());
        if (this.hasCustomName()) {
            compound.setString("CustomName", this.customName);
        }

        compound.setInteger("TemperatureStage", this.temperatureStage);
        compound.setBoolean("Burning", this.burning);

        return compound;
    }

    @Override
    public int getSizeInventory() {
        return this.stacks.length;
    }

    @Nullable
    @Override
    public ItemStack getStackInSlot(int index) {
        return this.stacks[index];
    }

    @Nullable
    @Override
    public ItemStack decrStackSize(int index, int count) {
        return ItemStackHelper.getAndSplit(this.stacks, index, count);
    }

    @Nullable
    @Override
    public ItemStack removeStackFromSlot(int index) {
        return ItemStackHelper.getAndRemove(this.stacks, index);
    }

    @Override
    public void setInventorySlotContents(int index, @Nullable ItemStack stack) {
        this.stacks[index] = stack;

        if (stack != null && stack.stackSize > this.getInventoryStackLimit()) {
            stack.stackSize = this.getInventoryStackLimit();
        }

        if (index == SLOT_INPUT) {
            this.updateTotalCookTime();
        }
    }

    @Override
    public int getInventoryStackLimit() {
        return 64;
    }

    @Override
    public boolean isUseableByPlayer(EntityPlayer player) {
        return this.worldObj.getTileEntity(this.pos) == this && player.getDistanceSq(this.pos.getX() + 0.5D,
                this.pos.getY() + 0.5D, this.pos.getZ() + 0.5D) <= 64.0D;
    }

    @Override
    public void openInventory(EntityPlayer player) {

    }

    @Override
    public void closeInventory(EntityPlayer player) {

    }

    @Override
    public boolean isItemValidForSlot(int index, ItemStack stack) {
        return index == SLOT_INPUT;
    }

    @Override
    public int getField(int id) {
        switch (id)
        {
            case FIELD_MAX_FUEL_UNITS:
                return this.fuelUnits.getMaxAmount();
            case FIELD_FUEL_UNITS:
                return this.fuelUnits.getAmount();
            case FIELD_MAX_TEMPERATURE:
                return this.temperature.getMaxAmount();
            case FIELD_TEMPERATURE:
                return this.temperature.getAmount();
            case FIELD_MAX_COOK_TIME:
                return this.cookTime.getMaxAmount();
            case FIELD_COOK_TIME:
                return this.cookTime.getAmount();
            default:
                return 0;
        }
    }

    @Override
    public void setField(int id, int value) {
        switch (id) {
            case FIELD_MAX_FUEL_UNITS:
                this.fuelUnits.unsafeSetMaxAmount(value);
                break;
            case FIELD_FUEL_UNITS:
                this.fuelUnits.unsafeSetAmount(value);
                break;
            case FIELD_MAX_TEMPERATURE:
                this.temperature.unsafeSetMaxAmount(value);
                break;
            case FIELD_TEMPERATURE:
                this.temperature.unsafeSetAmount(value);
                break;
            case FIELD_MAX_COOK_TIME:
                this.cookTime.unsafeSetMaxAmount(value);
                break;
            case FIELD_COOK_TIME:
                this.cookTime.unsafeSetAmount(value);
                break;
        }
    }

    @Override
    public int getFieldCount() {
        return 6;
    }

    @Override
    public void clear() {
        for (int i = 0; i < this.stacks.length; i++) {
            this.stacks[i] = null;
        }
    }

    @Override
    public Container createContainer(InventoryPlayer playerInventory, EntityPlayer player) {
        return new ContainerGasFurnace(playerInventory, this);
    }

    @Override
    public String getGuiID() {
        return GasesFramework.MODID + ":gasFurnace";
    }

    @Override
    public String getName() {
        return this.hasCustomName() ? this.customName : "container.gasFurnace";
    }

    @Override
    public boolean hasCustomName() {
        return this.customName != null && !this.customName.isEmpty();
    }

    @Override
    public int[] getSlotsForFace(EnumFacing side) {
        switch (side) {
            case DOWN:
                return SLOTS_BOTTOM;
            case UP:
                return SLOTS_TOP;
            default:
                return SLOTS_SIDES;
        }
    }

    @Override
    public boolean canInsertItem(int index, ItemStack stack, EnumFacing direction) {
        return this.isItemValidForSlot(index, stack);
    }

    @Override
    public boolean canExtractItem(int index, ItemStack stack, EnumFacing direction) {
        return true;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getCapability(Capability<T> capability, EnumFacing facing) {
        if (capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
            switch (facing) {
                case DOWN:
                    return (T)handlerBottom;
                case UP:
                    return (T)handlerTop;
                default:
                    return (T)handlerSide;
            }
        }
        return super.getCapability(capability, facing);
    }
}
