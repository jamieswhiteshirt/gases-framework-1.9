package com.jamieswhiteshirt.gasesframework.init;

import com.jamieswhiteshirt.gasesframework.GasesFramework;
import com.jamieswhiteshirt.gasesframework.block.BlockIronGasFurnace;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.fml.common.registry.GameRegistry;

import java.lang.reflect.Field;

public class GFBlocks {
    public static final Block TEST_BLOCK = new Block(Material.ROCK).setUnlocalizedName("testBlock");
    public static final BlockIronGasFurnace GAS_FURNACE_IRON_IDLE = new BlockIronGasFurnace();
    public static final BlockIronGasFurnace GAS_FURNACE_IRON_BURNING = new BlockIronGasFurnace();

    public static void registerBlocks() {
        // Iterate over fields that are blocks
        for (Field field : GFBlocks.class.getFields()) {
            if (Block.class.isAssignableFrom(field.getType())) {
                try {
                    // Set the registry name to the field name in lower case and register it
                    String registryName = field.getName().toLowerCase();
                    Block block = (Block)field.get(null);
                    block.setRegistryName(GasesFramework.MODID, registryName);
                    GameRegistry.register(block);

                    // Manually register ItemBlock for the block with the same registry name
                    ItemBlock itemBlock = new ItemBlock(block);
                    itemBlock.setRegistryName(GasesFramework.MODID, registryName);
                    GameRegistry.register(itemBlock);
                }
                catch (IllegalAccessException e) {
                    throw new RuntimeException("Failed to register Block", e);
                }
            }
        }
    }
}
