package com.jamieswhiteshirt.gasesframework.init;

import com.jamieswhiteshirt.gasesframework.GasesFramework;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;

import java.lang.reflect.Field;

public class GFItems {
    public static final Item ADHESIVE = new Item().setUnlocalizedName("adhesive");

    public static void registerItems() {
        // Iterate over fields that are items
        for (Field field : GFItems.class.getFields()) {
            if (Item.class.isAssignableFrom(field.getType())) {
                try {
                    // Set the registry name to the field name in lower case and register it
                    String registryName = field.getName().toLowerCase();
                    Item item = (Item)field.get(null);
                    item.setRegistryName(GasesFramework.MODID, registryName);
                    GameRegistry.register(item);
                }
                catch (IllegalAccessException e) {
                    throw new RuntimeException("Failed to register Item", e);
                }
            }
        }
    }
}
