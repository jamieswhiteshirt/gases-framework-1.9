package com.jamieswhiteshirt.gasesframework.init;

import com.jamieswhiteshirt.gasesframework.GasesFramework;
import com.jamieswhiteshirt.gasesframework.common.network.GuiHandler;
import com.jamieswhiteshirt.gasesframework.tileentity.TileEntityIronGasFurnace;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class GFBootstrap {
    public static void register() {
        GFItems.registerItems();
        GFBlocks.registerBlocks();
        registerTileEntities();
        registerGuiHandler();
    }

    private static void registerTileEntities() {
        registerTileEntity(TileEntityIronGasFurnace.class, "IronGasFurnace");
    }

    private static void registerTileEntity(Class<? extends TileEntity> tileEntityClass, String name) {
        GameRegistry.registerTileEntity(tileEntityClass, GasesFramework.MODID + ":" + name);
    }

    private static void registerGuiHandler() {
        NetworkRegistry.INSTANCE.registerGuiHandler(GasesFramework.instance, new GuiHandler());
    }
}
