@ParametersAreNonnullByDefault
@MethodsReturnNonnullByDefault
package com.jamieswhiteshirt.gasesframework.client;

import mcp.MethodsReturnNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;