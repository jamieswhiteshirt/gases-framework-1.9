package com.jamieswhiteshirt.gasesframework.client.gui.inventory;

import com.jamieswhiteshirt.gasesframework.GasesFramework;
import com.jamieswhiteshirt.gasesframework.inventory.ContainerGasFurnace;
import com.jamieswhiteshirt.gasesframework.tileentity.TileEntityGasFurnaceBase;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.ResourceLocation;

public class GuiGasFurnace extends GuiContainer {
    private static final ResourceLocation GAS_FURNACE_GUI_TEXTURES = new ResourceLocation(GasesFramework.MODID,
            "textures/gui/container/gas_furnace.png");
    private final InventoryPlayer playerInventory;
    private IInventory furnaceInventory;

    public GuiGasFurnace(InventoryPlayer playerInventory, IInventory furnaceInventory) {
        super(new ContainerGasFurnace(playerInventory, furnaceInventory));
        this.playerInventory = playerInventory;
        this.furnaceInventory = furnaceInventory;
    }

    private boolean isSmelting() {
        return this.furnaceInventory.getField(TileEntityGasFurnaceBase.FIELD_TEMPERATURE) > 0;
    }

    private int getTemperatureStage() {
        int temperature = this.furnaceInventory.getField(TileEntityGasFurnaceBase.FIELD_TEMPERATURE);
        int maxTemperature = this.furnaceInventory.getField(TileEntityGasFurnaceBase.FIELD_MAX_TEMPERATURE);
        return maxTemperature > 0 ? (4 * temperature / maxTemperature) : 0;
    }

    private int getTemperatureSubStageScaled(int pixels) {
        int temperature = this.furnaceInventory.getField(TileEntityGasFurnaceBase.FIELD_TEMPERATURE);
        int maxTemperature = this.furnaceInventory.getField(TileEntityGasFurnaceBase.FIELD_MAX_TEMPERATURE);
        return pixels * (4 * temperature % maxTemperature) / maxTemperature;
    }

    private boolean isBurning() {
        return this.furnaceInventory.getField(TileEntityGasFurnaceBase.FIELD_FUEL_UNITS) > 0;
    }

    private int getFuelUnitsScaled(int pixels) {
        int fuelUnits = this.furnaceInventory.getField(TileEntityGasFurnaceBase.FIELD_FUEL_UNITS);
        int maxFuelUnits = this.furnaceInventory.getField(TileEntityGasFurnaceBase.FIELD_MAX_FUEL_UNITS);
        return maxFuelUnits > 0 ? pixels * fuelUnits / maxFuelUnits : 0;
    }

    private int getCookTimeScaled(int pixels) {
        int cookTime = this.furnaceInventory.getField(TileEntityGasFurnaceBase.FIELD_COOK_TIME);
        int maxCookTime = this.furnaceInventory.getField(TileEntityGasFurnaceBase.FIELD_MAX_COOK_TIME);
        return maxCookTime > 0 ? pixels * cookTime / maxCookTime : 0;
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        String furnaceInventoryName = this.furnaceInventory.getDisplayName().getUnformattedText();
        this.fontRendererObj.drawString(furnaceInventoryName, this.xSize / 2 -
                this.fontRendererObj.getStringWidth(furnaceInventoryName) / 2, 6, 0x404040);

        String playerInventoryName = this.playerInventory.getDisplayName().getUnformattedText();
        this.fontRendererObj.drawString(playerInventoryName, 8, this.ySize - 96 + 2, 0x404040);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.getTextureManager().bindTexture(GAS_FURNACE_GUI_TEXTURES);
        int x = (this.width - this.xSize) / 2;
        int y = (this.height - this.ySize) / 2;
        this.drawTexturedModalRect(x, y, 0, 0, this.xSize, this.ySize);

        if (this.isSmelting()) {
            int temperatureStage = this.getTemperatureStage();
            int subStage = this.getTemperatureSubStageScaled(18);
            int subStageInverse = 18 - subStage;
            int uvX = 176 + (this.isBurning() ? 0 : 18);
            int uvY = 31 + temperatureStage * 18;
            this.drawTexturedModalRect(x + 55, y + 34, uvX, uvY, 18, subStageInverse);
            this.drawTexturedModalRect(x + 55, y + 34 + subStageInverse, uvX, uvY + 18 + subStageInverse, 18, subStage);
        }

        int fuelUnitsScaled = getFuelUnitsScaled(54);
        this.drawTexturedModalRect(x + 29, y + 70 - fuelUnitsScaled, 212, 54 - fuelUnitsScaled, 26, fuelUnitsScaled);

        int cookTimeScaled = this.getCookTimeScaled(24);
        this.drawTexturedModalRect(x + 79, y + 34, 176, 14, cookTimeScaled + 1, 16);
    }
}
