package com.jamieswhiteshirt.gasesframework.client;

import com.jamieswhiteshirt.gasesframework.CommonProxy;
import com.jamieswhiteshirt.gasesframework.GasesFramework;
import com.jamieswhiteshirt.gasesframework.init.GFBlocks;
import com.jamieswhiteshirt.gasesframework.init.GFItems;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.ItemModelMesher;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;

public class ClientProxy extends CommonProxy {
    @Override
    public void registerRenderers() {
        ItemModelMesher itemModelMesher = Minecraft.getMinecraft().getRenderItem().getItemModelMesher();
        itemModelMesher.register(GFItems.ADHESIVE, 0, new ModelResourceLocation(new ResourceLocation(GasesFramework.MODID, "adhesive"), "inventory"));
        itemModelMesher.register(Item.getItemFromBlock(GFBlocks.TEST_BLOCK), 0, new ModelResourceLocation(new ResourceLocation(GasesFramework.MODID, "test_block"), "inventory"));
        itemModelMesher.register(Item.getItemFromBlock(GFBlocks.GAS_FURNACE_IRON_IDLE), 0, new ModelResourceLocation(new ResourceLocation(GasesFramework.MODID, "gas_furnace_iron_idle"), "inventory"));
        itemModelMesher.register(Item.getItemFromBlock(GFBlocks.GAS_FURNACE_IRON_BURNING), 0, new ModelResourceLocation(new ResourceLocation(GasesFramework.MODID, "gas_furnace_iron_burning"), "inventory"));
    }
}
