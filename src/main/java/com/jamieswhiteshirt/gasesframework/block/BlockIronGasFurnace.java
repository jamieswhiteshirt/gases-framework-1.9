package com.jamieswhiteshirt.gasesframework.block;

import com.jamieswhiteshirt.gasesframework.init.GFBlocks;
import com.jamieswhiteshirt.gasesframework.tileentity.TileEntityGasFurnaceBase;
import com.jamieswhiteshirt.gasesframework.tileentity.TileEntityIronGasFurnace;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockIronGasFurnace extends BlockGasFurnaceBase {
    public BlockIronGasFurnace() {
        super(Material.IRON);
    }

    @Override
    public void onEntityWalk(World world, BlockPos pos, Entity entity) {
        TileEntityGasFurnaceBase tileEntity = this.getFurnaceTileEntity(world, pos);
        if (tileEntity != null) {
            if (tileEntity.getTemperatureStage() >= 3) {
                entity.attackEntityFrom(DamageSource.inFire, 1.0F);
            }
        }
    }

    @Override
    protected TileEntityGasFurnaceBase createNewTileEntityGasFurnace(World world, int meta) {
        return new TileEntityIronGasFurnace();
    }

    @Override
    public BlockGasFurnaceBase getIdleBlock() {
        return GFBlocks.GAS_FURNACE_IRON_IDLE;
    }

    @Override
    public BlockGasFurnaceBase getBurningBlock() {
        return GFBlocks.GAS_FURNACE_IRON_BURNING;
    }
}
