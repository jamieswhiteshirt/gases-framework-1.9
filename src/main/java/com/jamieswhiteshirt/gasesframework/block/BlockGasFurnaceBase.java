package com.jamieswhiteshirt.gasesframework.block;

import com.jamieswhiteshirt.gasesframework.GasesFramework;
import com.jamieswhiteshirt.gasesframework.inventory.ContainerGasFurnace;
import com.jamieswhiteshirt.gasesframework.tileentity.TileEntityGasFurnaceBase;
import com.jamieswhiteshirt.util.orientation.*;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyInteger;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import javax.annotation.Nullable;

public abstract class BlockGasFurnaceBase extends BlockOriented implements ITileEntityProvider {
    // Block events
    public static final int BLOCK_EVENT_SET_IS_BURNING = 0;
    public static final int BLOCK_EVENT_SET_TEMPERATURE_STAGE = 1;

    public static final PropertyOrientation ORIENTATION = BlockOriented.ORIENTATION;
    public static final PropertyInteger TEMPERATURE_STAGE = PropertyInteger.create("temperature_stage", 0, 3);

    public BlockGasFurnaceBase(Material material) {
        super(material);
    }

    /**
     * Triggered by the tile entity when the burning state changes.
     */
    public void onIsBurningChange(World world, BlockPos pos, TileEntityGasFurnaceBase tileEntity) {
        if (!world.isRemote) {
            IBlockState oldState = world.getBlockState(pos);
            BlockGasFurnaceBase newBlock = tileEntity.getBurning() ? this.getBurningBlock() : this.getIdleBlock();
            // Set new block with same orientation
            world.setBlockState(pos, newBlock.withOrientation(oldState.getValue(ORIENTATION)));
            tileEntity.validate();
            // The tile entity must be kept then re-placed when the block changes.
            world.setTileEntity(pos, tileEntity);
            world.addBlockEvent(pos, this, BLOCK_EVENT_SET_IS_BURNING, tileEntity.getBurning() ? 1 : 0);
        }
    }

    /**
     * Triggered by the tile entity when the temperature stage changes.
     */
    public void onTemperatureStageChange(World world, BlockPos pos, TileEntityGasFurnaceBase tileEntity) {
        if (!world.isRemote) {
            world.addBlockEvent(pos, this, BLOCK_EVENT_SET_TEMPERATURE_STAGE, tileEntity.getTemperatureStage());
        }
        else {
            world.markBlockRangeForRenderUpdate(pos, pos);
        }
    }

    /**
     * Get the furnace tile entity if it exists and is a TileEntityGasFurnaceBase.
     */
    @Nullable
    protected TileEntityGasFurnaceBase getFurnaceTileEntity(IBlockAccess world, BlockPos pos) {
        TileEntity tileEntity = world.getTileEntity(pos);
        if (tileEntity instanceof TileEntityGasFurnaceBase) {
            return (TileEntityGasFurnaceBase)tileEntity;
        }
        else {
            return null;
        }
    }

    @Override
    public TileEntity createNewTileEntity(World world, int meta) {
        return this.createNewTileEntityGasFurnace(world, meta);
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, @Nullable ItemStack heldItem, EnumFacing side, float hitX, float hitY, float hitZ) {
        if (world.isRemote) {
            return true;
        }
        else {
            TileEntity tileEntity = this.getFurnaceTileEntity(world, pos);
            if (tileEntity != null) {
                player.openGui(GasesFramework.instance, ContainerGasFurnace.GUI_ID, world, pos.getX(), pos.getY(),
                        pos.getZ());
            }

            return true;
        }
    }

    @Override
    public void breakBlock(World world, BlockPos pos, IBlockState state) {
        TileEntityGasFurnaceBase tileEntity = this.getFurnaceTileEntity(world, pos);
        if (tileEntity != null) {
            // The block is considered to be "broken" when replaced during a idle/burning change, therefore this event
            // fires. This bypasses the default breaking behaviour during this transition.
            if (!tileEntity.isBurningChanging()) {
                InventoryHelper.dropInventoryItems(world, pos, tileEntity);
                world.updateComparatorOutputLevel(pos, this);
            }
        }

        super.breakBlock(world, pos, state);
    }

    @Override
    @SuppressWarnings("deprecation")
    public boolean hasComparatorInputOverride(IBlockState state)
    {
        return true;
    }

    @Override
    @SuppressWarnings("deprecation")
    public int getComparatorInputOverride(IBlockState blockState, World world, BlockPos pos) {
        return Container.calcRedstone(world.getTileEntity(pos));
    }

    @Override
    @SuppressWarnings("deprecation")
    public IBlockState getActualState(IBlockState state, IBlockAccess world, BlockPos pos) {
        TileEntityGasFurnaceBase tileEntity = this.getFurnaceTileEntity(world, pos);
        return state.withProperty(TEMPERATURE_STAGE, tileEntity != null ? tileEntity.getTemperatureStage() : 0);
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer(this, ORIENTATION, TEMPERATURE_STAGE);
    }

    @Override
    @SuppressWarnings("deprecation")
    public boolean eventReceived(IBlockState state, World world, BlockPos pos, int id, int param) {
        super.eventReceived(state, world, pos, id, param);
        TileEntityGasFurnaceBase tileEntity = this.getFurnaceTileEntity(world, pos);
        if (tileEntity != null) {
            switch (id) {
                case BLOCK_EVENT_SET_IS_BURNING:
                    tileEntity.setBurning(param > 0);
                    return true;
                case BLOCK_EVENT_SET_TEMPERATURE_STAGE:
                    tileEntity.setTemperatureStage(param);
                    return true;
            }
        }

        return false;
    }

    /**
     * Create a TileEntityGasFurnaceBase for this block.
     */
    protected abstract TileEntityGasFurnaceBase createNewTileEntityGasFurnace(World world, int meta);

    /**
     * Get the block to be used when the gas furnace is idle.
     */
    abstract BlockGasFurnaceBase getIdleBlock();

    /**
     * Get the block to be used when the gas furnace is burning.
     */
    abstract BlockGasFurnaceBase getBurningBlock();
}
