package com.jamieswhiteshirt.gasesframework;

import com.jamieswhiteshirt.gasesframework.config.GFConfiguration;
import com.jamieswhiteshirt.gasesframework.init.GFBootstrap;
import com.jamieswhiteshirt.gasesframework.item.crafting.GasFurnaceRecipes;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import org.apache.logging.log4j.Logger;

@Mod(modid = GasesFramework.MODID, version = GasesFramework.VERSION)
public class GasesFramework {
    public static final String MODID = "gasesframework";
    public static final String VERSION = "2.0.0";
    public static final String MCVERSION = "1.9.4";

    @Mod.Instance
    public static GasesFramework instance;

    @SidedProxy(clientSide = "com.jamieswhiteshirt.gasesframework.client.ClientProxy",
            serverSide = "com.jamieswhiteshirt.gasesframework.server.ServerProxy", modId = MODID)
    public static CommonProxy proxy;

    public static Logger logger;
    public static GFConfiguration configuration;

    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        logger = event.getModLog();
        configuration = new GFConfiguration(logger);
        configuration.initialize(event.getSuggestedConfigurationFile());

        GFBootstrap.register();
    }

    @EventHandler
    public void init(FMLInitializationEvent event) {
        proxy.registerRenderers();

        GasFurnaceRecipes.initialize(configuration);
    }
}
