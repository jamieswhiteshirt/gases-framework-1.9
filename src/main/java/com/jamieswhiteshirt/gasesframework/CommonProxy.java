package com.jamieswhiteshirt.gasesframework;

public abstract class CommonProxy {
    public abstract void registerRenderers();
}
