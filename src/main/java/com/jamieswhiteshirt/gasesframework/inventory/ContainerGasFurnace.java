package com.jamieswhiteshirt.gasesframework.inventory;

import com.jamieswhiteshirt.gasesframework.item.crafting.GasFurnaceRecipes;
import com.jamieswhiteshirt.gasesframework.tileentity.TileEntityGasFurnaceBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.*;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;

public class ContainerGasFurnace extends Container {
    public static final int GUI_ID = 0;

    private final IInventory furnaceInventory;
    private final int[] values;

    public ContainerGasFurnace(InventoryPlayer playerInventory, IInventory furnaceInventory) {
        this.furnaceInventory = furnaceInventory;
        this.values = new int[furnaceInventory.getFieldCount()];
        this.addSlotToContainer(new Slot(furnaceInventory, TileEntityGasFurnaceBase.SLOT_INPUT, 56, 17));
        this.addSlotToContainer(new SlotFurnaceOutput(playerInventory.player, furnaceInventory,
                TileEntityGasFurnaceBase.SLOT_OUTPUT, 116, 35));

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 9; j++) {
                this.addSlotToContainer(new Slot(playerInventory, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
            }
        }

        for (int k = 0; k < 9; k++) {
            this.addSlotToContainer(new Slot(playerInventory, k, 8 + k * 18, 142));
        }
    }

    @Override
    public void addListener(IContainerListener listener) {
        super.addListener(listener);
        listener.sendAllWindowProperties(this, this.furnaceInventory);
    }

    @Override
    public void detectAndSendChanges() {
        super.detectAndSendChanges();

        for (IContainerListener listener : this.listeners) {
            for (int i = 0; i < this.values.length; i++) {
                if (this.values[i] != this.furnaceInventory.getField(i)) {
                    listener.sendProgressBarUpdate(this, i, this.furnaceInventory.getField(i));
                }
            }
        }

        for (int i = 0; i < this.values.length; i++) {
            this.values[i] = this.furnaceInventory.getField(i);
        }
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void updateProgressBar(int id, int data) {
        this.furnaceInventory.setField(id, data);
    }

    @Override
    public boolean canInteractWith(EntityPlayer player) {
        return this.furnaceInventory.isUseableByPlayer(player);
    }

    @Override
    @Nullable
    public ItemStack transferStackInSlot(EntityPlayer player, int index) {
        ItemStack immutableStack = null;
        Slot slot = this.inventorySlots.get(index);

        if (slot != null && slot.getHasStack()) {
            ItemStack mutableStack = slot.getStack();
            immutableStack = mutableStack.copy();

            if (index == 0) {
                //This is from the furnace input slot
                if (!this.mergeItemStack(mutableStack, 2, 38, false)) {
                    return null;
                }
            }
            else if (index == 1) {
                //Taking from the furnace output slot
                if (!this.mergeItemStack(mutableStack, 2, 38, true)) {
                    return null;
                }

                slot.onSlotChange(mutableStack, immutableStack);
            }
            else {
                //Taking from an item somewhere in the player's inventory to be placed somewhere in the furnace
                int temperature = furnaceInventory.getField(TileEntityGasFurnaceBase.FIELD_TEMPERATURE);
                if (GasFurnaceRecipes.getSmeltingResult(mutableStack, temperature) != null) {
                    //This is smeltable
                    if (!this.mergeItemStack(mutableStack, 0, 1, false))
                    {
                        return null;
                    }
                }
                else if (index >= 2 && index < 29) {
                    //This is from the player's main inventory
                    if (!this.mergeItemStack(mutableStack, 29, 38, false))
                    {
                        return null;
                    }
                }
                else if (index >= 29 && index < 38) {
                    //This is from the player's action bar
                    if (!this.mergeItemStack(mutableStack, 2, 29, false)) {
                        return null;
                    }
                }
            }

            if (mutableStack.stackSize == 0) {
                slot.putStack(null);
            }
            else {
                slot.onSlotChanged();
            }

            if (mutableStack.stackSize == immutableStack.stackSize) {
                return null;
            }

            slot.onPickupFromSlot(player, mutableStack);
        }

        return immutableStack;
    }
}
