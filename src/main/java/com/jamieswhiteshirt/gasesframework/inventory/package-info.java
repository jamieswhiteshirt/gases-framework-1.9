@ParametersAreNonnullByDefault
@MethodsReturnNonnullByDefault
package com.jamieswhiteshirt.gasesframework.inventory;

import mcp.MethodsReturnNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;