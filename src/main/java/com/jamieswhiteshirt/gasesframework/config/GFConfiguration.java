package com.jamieswhiteshirt.gasesframework.config;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.jamieswhiteshirt.gasesframework.item.crafting.GasFurnaceRecipes;
import com.jamieswhiteshirt.gasesframework.tileentity.TileEntityGasFurnaceBase;
import com.jamieswhiteshirt.util.configuration.Property;
import com.jamieswhiteshirt.util.configuration.ReflectionConfiguration;
import com.jamieswhiteshirt.util.configuration.aggregator.IAggregator;
import com.jamieswhiteshirt.util.configuration.aggregator.category.CategoryMapAggregator;
import com.jamieswhiteshirt.util.configuration.aggregator.category.StaticAggregator;
import org.apache.logging.log4j.Logger;

import java.util.Map;

public class GFConfiguration extends ReflectionConfiguration {
    public GFConfiguration(Logger logger) {
        super(logger);
    }

    @Property(comment = "Blocks", type = "category")
    public Blocks blocks = new Blocks();

    public static class Blocks {
        public static class GasFurnace implements TileEntityGasFurnaceBase.ISettings {
            @Property(comment = "The amount of ticks between each smoke emission", type = "int")
            public int emissionInterval;

            @Property(comment = "The maximal amount of fuel units contained by gas furnaces", type = "int")
            public int maxFuelUnits;

            @Property(comment = "The speed at which gas furnaces heat up", type = "int")
            public int temperaturePerFuelUnit;

            @Property(comment = "The speed at which gas furnaces cool down", type = "int")
            public int temperatureLostPerTick;

            @Property(comment = "The maximal temperature of gas furnaces", type = "int")
            public int maxTemperature;

            public GasFurnace(int emissionInterval, int maxFuelUnits, int temperaturePerFuelUnit, int temperatureLostPerTick, int maxTemperature) {
                this.emissionInterval = emissionInterval;
                this.maxFuelUnits = maxFuelUnits;
                this.temperaturePerFuelUnit = temperaturePerFuelUnit;
                this.temperatureLostPerTick = temperatureLostPerTick;
                this.maxTemperature = maxTemperature;
            }

            @Override
            public int getEmissionInterval() {
                return this.emissionInterval;
            }

            @Override
            public int getMaxFuelUnits() {
                return this.maxFuelUnits;
            }

            @Override
            public int getTemperaturePerFuelUnit() {
                return this.temperaturePerFuelUnit;
            }

            @Override
            public int getTemperatureLostPerTick() {
                return this.temperatureLostPerTick;
            }

            @Override
            public int getMaxTemperature() {
                return this.maxTemperature;
            }
        }

        @Property(comment = "Iron Gas Furnace", type = "category")
        public TileEntityGasFurnaceBase.ISettings ironGasFurnace = new GasFurnace(100, 1000, 4, 2, 8000);

        @Property(comment = "Wood Gas Furnace", type = "category")
        public TileEntityGasFurnaceBase.ISettings woodGasFurnace = new GasFurnace(75, 1000, 4, 3, 8000);
    }

    public static class GasFurnaceRecipeValue implements GasFurnaceRecipes.IRecipeValue {
        @Property(comment = "The time it takes to smelt the item", type = "int")
        public int smeltingTime;

        @Property(comment = "The amount of experience gained from smelting", type = "float")
        public float experience;

        @Property(comment = "The resulting item from smelting", type = "string")
        public String result;

        public GasFurnaceRecipeValue(int smeltingTime, float experience, String result) {
            this.smeltingTime = smeltingTime;
            this.experience = experience;
            this.result = result;
        }

        @Override
        public int getSmeltingTime() {
            return smeltingTime;
        }

        @Override
        public float getExperience() {
            return experience;
        }

        @Override
        public String getResult() {
            return result;
        }
    }

    @Property(
            comment = "Gas furnace recipes.\n" +
                    "Syntax (* is a wildcard):\n" +
                    "\"<item> [amount=1] [data=*] [temperature=0]\": {\n" +
                    "    D:experience=,\n" +
                    "    S:result=\"<item> [amount=1] [data=0]\"\n" +
                    "    I:smeltingTime=\n" +
                    "}\n" +
                    "\n" +
                    "Example:\n" +
                    "\"minecraft:coal 64 * 1000\": {\n" +
                    "    D:experience=800.0,\n" +
                    "    S:result=\"minecraft:diamond\"\n" +
                    "    I:smeltingTime=256000\n" +
                    "}\n",
            type = "gasFurnaceRecipes"
    )
    public Map<String, GasFurnaceRecipes.IRecipeValue> gasFurnaceRecipes = Maps.newHashMap(
            ImmutableMap.<String, GasFurnaceRecipes.IRecipeValue>builder()
            .put("minecraft:coal 64", new GasFurnaceRecipeValue(256000, 800.0F, "minecraft:diamond"))
            .build()
    );

    @Override
    protected Map<String, IAggregator> getAggregators() {
        Map<String, IAggregator> aggregatorMap = super.getAggregators();

        aggregatorMap.put("gasFurnaceRecipes", new CategoryMapAggregator(new StaticAggregator()) {
            @Override
            protected Object createDefaultValue() {
                return new GasFurnaceRecipeValue(200, 0.0F, "");
            }
        });

        return aggregatorMap;
    }
}
