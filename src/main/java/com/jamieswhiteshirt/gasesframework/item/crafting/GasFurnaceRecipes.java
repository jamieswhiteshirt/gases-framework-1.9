package com.jamieswhiteshirt.gasesframework.item.crafting;

import com.jamieswhiteshirt.gasesframework.config.GFConfiguration;
import com.jamieswhiteshirt.gasesframework.item.crafting.smelting.SmeltingInput;
import com.jamieswhiteshirt.gasesframework.item.crafting.smelting.SmeltingMatcher;
import com.jamieswhiteshirt.gasesframework.item.crafting.smelting.SmeltingMatcherParser;
import com.jamieswhiteshirt.gasesframework.item.crafting.smelting.SmeltingRecipe;
import com.jamieswhiteshirt.util.misc.parser.ItemStackParser;
import com.jamieswhiteshirt.util.misc.parser.ParserException;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Tuple;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class GasFurnaceRecipes {
    public interface IRecipeValue {
        String getResult();

        int getSmeltingTime();

        float getExperience();
    }

    public static SmeltingRecipe smeltingResultFromRecipeValue(int inputStackSize, IRecipeValue recipe)
            throws ParserException {
        String[] outputStackParameters = recipe.getResult().split(" ");
        ItemStack outputStack = ItemStackParser.INSTANCE.parse(outputStackParameters);
        return new SmeltingRecipe(inputStackSize, outputStack, recipe.getSmeltingTime(), recipe.getExperience());
    }

    private static final List<Tuple<SmeltingMatcher, SmeltingRecipe>> SMELTING_LIST =
            new ArrayList<Tuple<SmeltingMatcher, SmeltingRecipe>>();

    public static void initialize(GFConfiguration configuration) {
        for (Map.Entry<String, IRecipeValue> entry : configuration.gasFurnaceRecipes.entrySet()) {
            String key = entry.getKey().trim();
            if (key.length() == 0) {
                configuration.blame("Empty smelting item definition in gasFurnaceRecipes");
                continue;
            }

            try {
                SmeltingMatcher matcher = SmeltingMatcherParser.INSTANCE.parse(key);
                SmeltingRecipe result = smeltingResultFromRecipeValue(matcher.stack.stackSize, entry.getValue());
                SMELTING_LIST.add(new Tuple<SmeltingMatcher, SmeltingRecipe>(matcher, result));
            }
            catch (ParserException e) {
                configuration.blame("Invalid smelting item definition \"%s\" in gasFurnaceRecipes (%s)", key,
                        e.getMessage());
            }
        }
    }

    @Nullable
    public static SmeltingRecipe getSmeltingResult(ItemStack stack, int temperature) {
        SmeltingInput input = new SmeltingInput(stack, temperature);
        Tuple<SmeltingMatcher, SmeltingRecipe> result = null;
        for (Tuple<SmeltingMatcher, SmeltingRecipe> entry : SMELTING_LIST) {
            if (entry.getFirst().match(input)) {
                // This recipe is compatible with the input.
                if (result == null || result.getFirst().compareTo(entry.getFirst()) > 0) {
                    // This recipe takes precedence over the other recipe
                    result = entry;
                }
            }
        }

        if (result != null) {
            return result.getSecond();
        }
        else {
            return SmeltingRecipe.fromVanillaSmeltingRecipe(stack);
        }
    }
}
