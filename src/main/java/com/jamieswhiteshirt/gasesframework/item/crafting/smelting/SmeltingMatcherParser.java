package com.jamieswhiteshirt.gasesframework.item.crafting.smelting;

import com.jamieswhiteshirt.util.misc.matcher.IMatcher;
import com.jamieswhiteshirt.util.misc.matcher.IngredientItemStackMatcher;
import com.jamieswhiteshirt.util.misc.matcher.IntMatcher;
import com.jamieswhiteshirt.util.misc.parser.ParserException;
import com.jamieswhiteshirt.util.misc.parser.ParserUtil;
import com.jamieswhiteshirt.util.misc.parser.SplitParserBase;
import com.jamieswhiteshirt.util.misc.parser.matcher.IngredientItemStackMatcherParser;
import com.jamieswhiteshirt.util.misc.parser.matcher.IntMatcherParser;

public class SmeltingMatcherParser extends SplitParserBase<SmeltingMatcher> {
    public static final SmeltingMatcherParser INSTANCE = new SmeltingMatcherParser();

    private SmeltingMatcherParser() {

    }

    @Override
    public SmeltingMatcher parse(String[] values) throws ParserException {
        IngredientItemStackMatcher stack = IngredientItemStackMatcherParser.INSTANCE.parse(ParserUtil.copyAtMost(values,
                3));
        IMatcher<Integer> temperature = values.length > 3 ? IntMatcherParser.INSTANCE.parse(values[3]) :
                IntMatcher.Any.INSTANCE;

        return new SmeltingMatcher(stack, temperature);
    }
}
