package com.jamieswhiteshirt.gasesframework.item.crafting.smelting;

import com.jamieswhiteshirt.util.misc.matcher.IMatcher;
import com.jamieswhiteshirt.util.misc.matcher.IngredientItemStackMatcher;

public class SmeltingMatcher implements Comparable<SmeltingMatcher>, IMatcher<SmeltingInput> {
    public final IngredientItemStackMatcher stack;
    public final IMatcher<Integer> temperature;

    public SmeltingMatcher(IngredientItemStackMatcher stack, IMatcher<Integer> temperature) {
        this.stack = stack;
        this.temperature = temperature;
    }

    @Override
    public boolean match(SmeltingInput value) {
        return this.stack.match(value.stack) && this.temperature.match(value.temperature);
    }

    @Override
    public String toString() {
        return this.stack + " " + this.temperature;
    }

    @Override
    public int compareTo(SmeltingMatcher o) {
        return stack.compareTo(o.stack);
    }
}
