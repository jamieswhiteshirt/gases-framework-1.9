package com.jamieswhiteshirt.gasesframework.item.crafting.smelting;

import net.minecraft.item.ItemStack;

public class SmeltingInput {
    public final ItemStack stack;
    public final int temperature;

    public SmeltingInput(ItemStack stack, int temperature) {
        this.stack = stack;
        this.temperature = temperature;
    }
}
