package com.jamieswhiteshirt.gasesframework.item.crafting.smelting;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;

import javax.annotation.Nullable;

public class SmeltingRecipe {
    public final int inputStackSize;
    public final ItemStack outputStack;
    public final int smeltingTime;
    public final float experience;

    public SmeltingRecipe(int inputStackSize, ItemStack outputStack, int smeltingTime, float experience) {
        this.inputStackSize = inputStackSize;
        this.smeltingTime = smeltingTime;
        this.outputStack = outputStack;
        this.experience = experience;
    }

    @Nullable
    public static SmeltingRecipe fromVanillaSmeltingRecipe(ItemStack stack) {
        ItemStack output = FurnaceRecipes.instance().getSmeltingResult(stack);
        if (output != null) {
            float experience = FurnaceRecipes.instance().getSmeltingExperience(stack);
            return new SmeltingRecipe(1, output, 200, experience);
        }

        return null;
    }
}
