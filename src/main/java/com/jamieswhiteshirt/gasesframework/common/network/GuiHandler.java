package com.jamieswhiteshirt.gasesframework.common.network;

import com.jamieswhiteshirt.gasesframework.client.gui.inventory.GuiGasFurnace;
import com.jamieswhiteshirt.gasesframework.inventory.ContainerGasFurnace;
import com.jamieswhiteshirt.gasesframework.tileentity.TileEntityGasFurnaceBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

public class GuiHandler implements IGuiHandler {
    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        TileEntity tileEntity = world.getTileEntity(new BlockPos(x, y, z));
        if (tileEntity != null) {
            switch (ID) {
                case ContainerGasFurnace.GUI_ID:
                    return new ContainerGasFurnace(player.inventory, (TileEntityGasFurnaceBase)tileEntity);
            }
        }
        return null;
    }

    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        TileEntity tileEntity = world.getTileEntity(new BlockPos(x, y, z));
        if (tileEntity != null) {
            switch (ID) {
                case ContainerGasFurnace.GUI_ID:
                    return new GuiGasFurnace(player.inventory, (TileEntityGasFurnaceBase)tileEntity);
            }
        }
        return null;
    }
}
